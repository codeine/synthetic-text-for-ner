# Synthetic text for NER

For easier access, the repository for the EACL 2023 paper 'Can Synthetic Text Help Clinical Named Entity Recognition? A Study of Electronic Health Records in French' has been moved to github.com. 

You'll find the repository containing the code and the corpora generated for the paper at this adress : https://github.com/Hiebel/Synthetic-text-for-NER


